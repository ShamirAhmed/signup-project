import React from "react";
import './css/Select.css'

class Select extends React.Component {
    render() {
        const style = {
            input: {
                border: '1px solid red'
            },
            msg: {
                display: 'block'
            }   
        }
        return (
            <>
                <div className="select">
                    <select id={this.props.id} style={this.props.validate ? {} : style.input}>
                        <option value=''>Select {this.props.label}</option>
                        {
                            this.props.option.split(',')
                                .map((data) => {
                                    return <option value={data}>{data}</option>
                                })
                        }
                    </select>
                    <span style={this.props.validate ? {} : style.msg}>{this.props.warning}</span>
                </div>
            </>
        );
    }
}

export default Select;