import React from 'react';
import './css/Input.css';

class Input extends React.Component {

    inputType = (event) => {
        if (this.props.inputType === 'text') {
            event.target.value = event.target.value.replace(/[^a-z]/gi, '')
        } else if (this.props.inputType === 'number') {
            event.target.value = event.target.value.replace(/[^0-9]/, '')
        }
    }

    render() {
        const style = {
            input: {
                border: '1px solid red'
            },
            msg: {
                display: 'block'
            }
        };
        return (
            this.props.type !== 'checkbox' ?
                <>
                    <div className='input-field'>
                        <input
                            type={this.props.type}
                            id={this.props.id}
                            placeholder={this.props.placeHolder}
                            onChange={this.inputType}
                            style={this.props.validate ? {} : style.input}
                        />
                        <i className={this.props.icon}></i>
                        <span style={this.props.validate ? {} : style.msg}>{this.props.warning}</span>
                    </div>
                </> : this.props.type === 'password' ?
                    <>
                        <div className='input-field'>
                            <div className='check'>
                                <input
                                    type={this.props.type}
                                    id={this.props.id}
                                    placeholder={this.props.placeHolder}
                                    onChange={this.inputType}
                                    style={this.props.validate ? {} : style.input}
                                />
                                <label>{this.props.label}</label>
                            </div>
                            <span style={this.props.validate ? {} : style.msg}>{this.props.warning}</span>
                        </div>
                    </> :
                    <>
                        <div className='input-field'>
                            <div className='check'>
                                <input
                                    type={this.props.type}
                                    id={this.props.id}
                                    placeholder={this.props.placeHolder}
                                    onChange={this.inputType}
                                    style={this.props.validate ? {} : style.input}
                                />
                                <label>{this.props.label}</label>
                            </div>
                            <span style={this.props.validate ? {} : style.msg}>{this.props.warning}</span>
                        </div>
                    </>
        );
    }
}

export default Input;