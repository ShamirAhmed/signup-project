import React from 'react';
import './css/Radio.css';

class Radio extends React.Component {
    render() {
        return (
            <>
                <label className='radio-label' htmlFor={this.props.id}>{this.props.label}</label>
                <input type='radio' id={this.props.id} className='radio-input' name={this.props.name} />
            </>
        );
    }
}

export default Radio;