import React from 'react';
import './css/Warning.css'

class Warning extends React.Component {
    render() {
        return (
            <>
                <span>{this.props.children}</span>
            </>
        );
    }
}

export default Warning;