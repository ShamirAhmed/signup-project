import './App.css';
import React from 'react';
import Input from './components/Input';
import Select from './components/Select';
import validator from 'validator';

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      firstName: true,
      lastName: true,
      age: true,
      gender: true,
      role: true,
      email: true,
      password: true,
      repeatPassword: true,
      checkBox: true,
      success: {
        display: 'none',
        color: 'green'
      }
    }
  }

  validate = (event) => {
    event.preventDefault();
    let flag = true;

    const firstName = event.target.firstName;
    const lastName = event.target.lastName;
    const age = event.target.age;
    const gender = event.target.gender;
    const role = event.target.role;
    const email = event.target.email;
    const password = event.target.password;
    const repeatPassword = event.target.repeatPassword;
    const checkBox = event.target.tos;

    // console.log(gender.value);


    if (!validator.isAlpha(firstName.value)) {
      flag = false;
      this.setState({
        firstName: false
      })
    } else {
      this.setState({
        firstName: true
      })
    }

    if (lastName.value === '') {
      flag = false;
      this.setState({
        lastName: false
      })
    } else {
      this.setState({
        lastName: true
      })
    }

    if (!validator.isInt(age.value)) {
      flag = false;
      this.setState({
        age: false
      })
    } else {
      this.setState({
        age: true
      })
    }

    if (gender.value === '') {
      flag = false;
      this.setState({
        gender: false
      })
    } else {
      this.setState({
        gender: true
      })
    }

    if (role.value === '') {
      flag = false;
      this.setState({
        role: false
      })
    } else {
      this.setState({
        role: true
      })
    }

    if (!validator.isEmail(email.value)) {
      flag = false;
      this.setState({
        email: false
      })
    } else {
      this.setState({
        email: true
      })
    }

    if (!validator.isStrongPassword(password.value,{
      minLength:8,
      minLowercase:0,
      minUppercase:0,
      minSymbols:0
    })) {
      flag = false;
      this.setState({
        password: false
      })
    } else {
      this.setState({
        password: true
      })
    }

    if (repeatPassword.value === '' || password.value !== repeatPassword.value) {
      flag = false;
      this.setState({
        repeatPassword: false
      })
    } else {
      this.setState({
        repeatPassword: true
      })
    }

    if (checkBox.checked === false) {
      flag = false;
      this.setState({
        checkBox: false
      })
    } else {
      this.setState({
        checkBox: true
      })
    }

    if(flag === true) {
      event.target.style.display = 'none';
      this.setState({
        success: {
          display:'block'
        }
      })
    }
  }

  render() {
    return (
      <>
        <h1 className='signup'>Sign Up</h1>
        <div style={this.state.success}>
          <h1>Your Form is successfully submitted</h1>
        </div>
        <div className='layout'>
          <form onSubmit={this.validate}>
            <Input
              type="text"
              id='firstName'
              warning='Please Enter Your First Name'
              placeHolder='First Name'
              inputType='text'
              validate={this.state.firstName}
              icon='fa-solid fa-user'
            />

            <Input
              type='text'
              id='lastName'
              warning='Please Enter Your Last Name'
              placeHolder='Last Name'
              inputType='text'
              validate={this.state.lastName}
              icon='fa-solid fa-user'
            />
            <Input
              type='text'
              id='age'
              warning='Please Enter Your Age'
              placeHolder='Age'
              inputType='number'
              validate={this.state.age}
              icon="fa-solid fa-arrow-up-9-1"
            />

            <Select
              id='gender'
              option='male,female,other'
              warning='Please Select your Gender'
              validate={this.state.gender}
              label='Gender'
            />

            <Select
              id='role'
              option='Developer,Senior Developer,Lead Engineer,CTO'
              warning='Please select Your Role'
              validate={this.state.role}
              label='Role'
            />

            <Input
              id='email'
              type='text'
              warning='Please Enter your Email'
              placeHolder='Email'
              validate={this.state.email}
              icon="fa-solid fa-at"
            />

            <Input
              id='password'
              type='password'
              warning='Password must contain atleast 8 charecters'
              placeHolder='Password'
              validate={this.state.password}
              icon="fa-solid fa-key"
            />

            <Input
              id='repeatPassword'
              type='password'
              warning='Password and Repeat Password does not match'
              placeHolder='Repeat Password'
              validate={this.state.repeatPassword}
              icon="fa-solid fa-key"  
            />

            <Input
              id='tos'
              type='checkbox'
              label='Agree to terms and conditions'
              warning='Please agree to Our Terms and Condition'
              validate={this.state.checkBox}
            />
            <button type='submit' className='submit-button'>Submit</button>
          </form>
          <div className='imageSide'></div>
        </div>
      </>
    );
  }
}

export default App;
